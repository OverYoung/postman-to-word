package com.zhexiao.convert.entity.postman;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author zhe.xiao
 * @date 2020/09/16
 * @description
 */
@Data
@Accessors(chain = true)
public class Item {
    private String name;

    private Request request;

    private List<Response> response;
}
