package com.zhexiao.convert.entity.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;

/**
 * @author zhe.xiao
 * @date 2021-01-20 15:47
 */
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ParagraphStyle implements Style {
    private ParagraphAlignment alignment;
    private Integer fontSize;
    private Boolean bold;
}
