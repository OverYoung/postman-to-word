package com.zhexiao.convert.controller;

import com.zhexiao.convert.base.Result;
import com.zhexiao.convert.entity.dto.ConvertDTO;
import com.zhexiao.convert.entity.postman.Postman;
import com.zhexiao.convert.service.impl.ConvertServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author zhe.xiao
 * @date 2020/09/15
 * @description
 */
@Api(tags = "转换数据")
@RestController
@RequestMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
public class ConvertController {

    @Autowired
    ConvertServiceImpl convertService;

    /**
     * 读取json字符串
     *
     * @param file
     * @return
     */
    @ApiOperation(value = "解析JSON文件")
    @PostMapping(value = "/upload")
    public Result<Postman> upload(MultipartFile file) {
        return convertService.upload(file);
    }

    @ApiOperation(value = "转换文件到WORD")
    @PostMapping(value = "/convert")
    public Result<String> writeWord(MultipartFile file, ConvertDTO convertDTO) {
        return convertService.writeWord(file, convertDTO);
    }
}
